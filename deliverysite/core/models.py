from django.db import models

# Create your models here.

from wagtail.core.models import Page
from wagtail.core.fields import StreamField
from wagtail.core import blocks
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel

class CorePage(Page):
    template = "core/core_template.html"
    text = models.CharField(verbose_name="Текстовое поле", max_length=80, null=True, blank=True)
    dt = models.DateTimeField(null=True, blank=True)


    body = StreamField([
        ('heading', blocks.CharBlock(classname="full title")),
        ('paragraph', blocks.RichTextBlock()),
    ], blank=True, null=True)




    content_panels = Page.content_panels + [
        FieldPanel('dt'),
        FieldPanel('text'),
        StreamFieldPanel('body'),
    ]


