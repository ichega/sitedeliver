from django.shortcuts import render
from django.contrib.auth import logout, login
from django.http.response import HttpResponseRedirect
# Create your views here.
from users.models import User

def logout_view(request):
    logout(request)
    print('KEK')
    return HttpResponseRedirect('/sign_in')