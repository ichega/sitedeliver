from django.db import models
import uuid
from wagtail.core.models import Page
from wagtail.core.fields import StreamField
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel, PageChooserPanel
from wagtail.images.blocks import ImageChooserBlock
from wagtail.snippets.edit_handlers import SnippetChooserPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.core import blocks
from django.shortcuts import redirect, render
from django.http.response import HttpResponseRedirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import AbstractUser
from wagtail.snippets.models import register_snippet
from users.models import User
from django.contrib.auth.hashers import make_password
import datetime, time
from wagtail.documents.models import Document
from wagtail.documents.edit_handlers import DocumentChooserPanel
from django import forms
from wagtail.documents.models import Document


@register_snippet
class ContactFormModel(models.Model):
    name = models.CharField(max_length=100, blank=True, verbose_name="Имя", null=True)
    email = models.EmailField(max_length=100, blank=True, verbose_name="Email", null=True)
    subject = models.CharField(max_length=200, blank=True, verbose_name="Тема", null=True)
    message = models.TextField(max_length=2000, blank=True, verbose_name="Сообщение", null=True)

    panels = [
        FieldPanel('name'),
        FieldPanel('email'),
        FieldPanel('subject'),
        FieldPanel('message')
    ]

    def __str__(self):
        return self.subject + " (" + self.email + ")"

    class Meta:
        verbose_name = "Форма контактов"
        verbose_name_plural = "Формы контактов"


@register_snippet
class SiteInfo(models.Model):
    phone = models.CharField(max_length=20, null=True, blank=True, verbose_name="Номер телефона")
    address = models.TextField(max_length=200, null=True, blank=True, verbose_name="Адрес")
    email = models.EmailField(max_length=40, null=True, blank=True, verbose_name="Email")

    panels = [
        FieldPanel('phone'),
        FieldPanel('address'),
        FieldPanel('email')
    ]

    def __str__(self):
        return str(self.phone) + ", " + str(self.address) + ", " + str(self.email)

    class Meta:
        verbose_name = "Информация о сайте"
        verbose_name_plural = "Информация о сайте"


#


# @register_snippet
# class Messages(models.Model):
#     user = models.ForeignKey(
#         User,
#         on_delete=models.SET_NULL,
#         related_name="+",
#         null=True,
#         blank=True,
#         verbose_name="Пользователь"
#     )
#     text = models.TextField(max_length=200, null=True, blank=True, verbose_name="Текст")
#     document = models.ForeignKey(
#         'Documents',
#         on_delete=models.CASCADE,
#         related_name='+', verbose_name="Документ",
#         null=True,
#         blank=True
#     )
#     panels = [
#         FieldPanel('user'),
#         FieldPanel('text'),
#         FieldPanel('document')
#     ]
#
#     def __str__(self):
#         return str(self.user) + ", " + str(self.text)
#
#     class Meta:
#         verbose_name = "Сообщение"
#         verbose_name_plural = "Сообщения"


class ChatPage(Page):
    template = "home/chat.html"

    def get_context(self, request):
        context = super().get_context(request)
        info = SiteInfo.objects.all()[0]
        context["info"] = info

        messages = []
        for message in Messages.objects.filter(user=request.user):
            doc = message.document
            mes = {
                "name_doc": doc.name,
                "link": doc.link,
                "text": message.text
            }
            messages.append(mes)
        context["messages"] = messages
        return context

    class Meta:
        verbose_name = "Страница описания услуги"
        verbose_name_plural = "Страницы описания услуг"


# class BlogPage(Page):
#
#     template = "core/test1.html"
#     date = models.DateField("Post date")
#
#     content_panels = Page.content_panels + [
#         FieldPanel('date')
#     ]
# class HomePage(Page):
#     pass


@register_snippet
class Service(models.Model):
    name = models.CharField(max_length=80, blank=True, null=True, verbose_name="Название")
    description = models.TextField(max_length=80, blank=True, null=True, verbose_name="Описание")
    image = models.ImageField(verbose_name="Изображение")
    rent = models.BooleanField(verbose_name='Да - Аренда, Нет - Доставка', default=True)
    link_page = models.ForeignKey(
        'wagtailcore.Page',

        on_delete=models.CASCADE,
        related_name='+', verbose_name="Расположить на странице"
    )

    panels = [
        FieldPanel('name'),
        FieldPanel('description'),
        FieldPanel('image'),
        FieldPanel('rent'),
        PageChooserPanel('link_page'),
        # PageChooserPanel('link_page'),

    ]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Услуга"
        verbose_name_plural = "Услуги"


@register_snippet
class ServiceOrder(models.Model):
    service = models.ForeignKey(
        Service,
        on_delete=models.SET_NULL,
        related_name="+",
        null=True,
        blank=True,
        verbose_name="Услуга"
    )
    user = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name="+",
        null=True,
        blank=True,
        verbose_name="Пользователь"
    )
    datetime = models.DateTimeField(verbose_name="Дата=время")
    place_from = models.CharField(max_length=80, verbose_name="Откуда")
    place_to = models.CharField(max_length=80, verbose_name="Куда")

    panels = [
        SnippetChooserPanel('service'),
        SnippetChooserPanel('user'),
        FieldPanel('datetime'),
        FieldPanel('place_from'),
        FieldPanel('place_to'),
    ]

    def __str__(self):
        return str(self.service.name) + f"(от {self.place_from} в {self.place_to} время: {self.datetime})"

    class Meta:
        verbose_name = "Заказ услуги"
        verbose_name_plural = "Заказы услуг"


class ProductPage(Page):
    template = "home/product.html"

    def serve(self, request):
        print(request.method)
        if request.method == 'GET':
            print('ARGS', request.GET)
            return super().serve(request)
        else:
            req = request.POST.copy()
            print(req)
            date = req['trip-start']
            place_from = req['place_from']
            place_to = req['place_to']
            # date = time.strptime(date, '%Y-%m-%d')
            id = req['id']
            service = Service.objects.get(id=id)
            order = ServiceOrder(
                service=service,
                user=request.user,
                datetime=date,
                place_from=place_from,
                place_to=place_to,
            )
            order.save()

            return HttpResponseRedirect('/rent')

    def get_context(self, request):
        context = super().get_context(request)
        info = SiteInfo.objects.all()[0]
        context["info"] = info
        id = request.GET['id']
        context["service"] = Service.objects.get(id=id)
        return context

    class Meta:
        verbose_name = "Страница описания услуги"
        verbose_name_plural = "Страницы описания услуг"


class MainPage(Page):
    template = "home/index.html"
    slider = StreamField([
        ('slider', blocks.ListBlock(
            blocks.StructBlock([
                ('text', blocks.RichTextBlock()),
                ('image', ImageChooserBlock()),
            ])
        ))

    ], null=True, blank=True)
    delivery_text = models.TextField(max_length=1000, verbose_name="Доставка", null=True, blank=True)
    delivery_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+")
    rent_text = models.TextField(max_length=1000, verbose_name="Аренда", null=True, blank=True)
    rent_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+")
    text1_title = models.CharField(max_length=1000, verbose_name="Заголовок", null=True, blank=True)
    text1 = models.TextField(max_length=1000, verbose_name="Текст", null=True, blank=True)

    why_me = models.TextField(max_length=1000, verbose_name="Почему мы?", null=True, blank=True)

    advantages = StreamField([

        ('slider', blocks.ListBlock(
            blocks.StructBlock([
                ('name', blocks.CharBlock(required=False)),
                ('title', blocks.CharBlock(required=False)),
                ('image', ImageChooserBlock(required=False)),
                ('text', blocks.RichTextBlock(required=False)),
                ('icom_class', blocks.CharBlock(required=False)),
                ('id', blocks.CharBlock(required=False)),
            ])
        ))

    ], null=True, blank=True)
    services = StreamField([
        ('service', blocks.ListBlock(
            blocks.StructBlock([
                ('name', blocks.CharBlock(required=False)),
                ('description', blocks.CharBlock(required=False)),
                ('image', ImageChooserBlock(required=False)),
                # ('text', blocks.RichTextBlock(required=False)),
                # ('icom_class', blocks.CharBlock(required=False)),
                ('page', blocks.PageChooserBlock(required=False)),
            ])
        ))

    ], null=True, blank=True)
    slider2 = StreamField([
        ('slider', blocks.ListBlock(

            ImageChooserBlock(),

        ))

    ], null=True, blank=True)
    # contacts = models.TextField(max_length=100, verbose_name="контакты", null=True, blank=True)

    content_panels = Page.content_panels + [
        StreamFieldPanel('slider'),
        FieldPanel('delivery_text'),
        ImageChooserPanel('delivery_image'),
        FieldPanel('rent_text'),
        ImageChooserPanel('rent_image'),
        FieldPanel('text1_title'),
        FieldPanel('text1'),
        FieldPanel('why_me'),
        StreamFieldPanel('advantages'),
        StreamFieldPanel('services'),
        StreamFieldPanel('slider2'),
        # FieldPanel('contacts'),

    ]

    def get_context(self, request):
        context = super().get_context(request)
        info = SiteInfo.objects.all()[0]
        context["info"] = info
        return context

    class Meta:
        verbose_name = "Главная"
        verbose_name_plural = "Главные"


class ServicePage(Page):
    template = "home/services.html"
    parent = models.ForeignKey(Page, on_delete=models.SET_NULL, null=True, blank=True, verbose_name="Страница-предок",
                               related_name="+")
    image = models.ImageField(null=True, blank=True, verbose_name="Изображение")
    services = StreamField([
        ('list', blocks.ListBlock(
            blocks.StructBlock([
                ('name', blocks.CharBlock(required=False)),
                ('image', ImageChooserBlock(required=False)),
            ])
        ))
    ], null=True, blank=True)
    title_text = models.CharField(max_length=1000, verbose_name="Заголовок", null=True, blank=True)
    text = models.TextField(max_length=1000, verbose_name="Текст", null=True, blank=True)

    content_panels = Page.content_panels + [
        PageChooserPanel('parent'),
        FieldPanel('image'),
        FieldPanel('title_text'),
        FieldPanel('text'),
        StreamFieldPanel('services'),

    ]

    def get_context(self, request):
        context = super().get_context(request)
        info = SiteInfo.objects.all()[0]
        context["info"] = info
        services = Service.objects.filter(link_page=self)
        context["services"] = services
        subsections = ServicePage.objects.filter(parent=self)
        context["subsections"] = subsections
        return context

    class Meta:
        verbose_name = "Услуга"
        verbose_name_plural = "Услуги"


class Delivery(Page):
    template = "core/test4.html"
    deliveries = StreamField([
        ('name', blocks.CharBlock(required=False)),
        ('text', blocks.RichTextBlock(required=False)),
        ('image', ImageChooserBlock(required=False)),
    ], null=True, blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('deliveries'),
    ]


@register_snippet
class Review(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, verbose_name="Пользователь")
    text = models.TextField(max_length=3000, verbose_name='Текст отзыва')

    panels = [
        FieldPanel('user'),
        FieldPanel('text'),
    ]

    def __str__(self):
        return str(self.user.email)

    class Meta:
        verbose_name = "Обзор"
        verbose_name_plural = "Обзоры"


class Reviews(Page):
    template = "home/reviews.html"
    text = models.CharField(max_length=200, verbose_name='Текст', null=True, blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('text'),
    ]

    def get_context(self, request):
        context = super().get_context(request)
        info = SiteInfo.objects.all()[0]
        reviews = Review.objects.all()
        context["info"] = info
        context['reviews'] = reviews
        return context

    def serve(self, request):
        print(request.method)
        if request.method == 'GET':
            return super().serve(request)
        else:
            req = request.POST.copy()
            print(req)
            if 'message' in req:
                message = req['message']
                review = Review(user=request.user, text=message)
                review.save()
                return HttpResponseRedirect('/reviews')

            return HttpResponseRedirect('/reviews')

    class Meta:
        verbose_name = "Обзор"
        verbose_name_plural = "Обзоры"


class AboutCompany(Page):
    template = "home/about.html"
    info = models.TextField(max_length=4000, verbose_name="Информация", null=True, blank=True)
    banner = models.ImageField(blank=True, null=True, verbose_name='Картинка')

    our_values = models.TextField(max_length=1000, null=True, blank=True, verbose_name='Текст')
    our_mission = models.TextField(max_length=1000, null=True, blank=True, verbose_name='Текст')
    our_vission = models.TextField(max_length=1000, null=True, blank=True, verbose_name='Текст')

    content_panels = Page.content_panels + [
        FieldPanel('info'),
        FieldPanel('banner'),

        FieldPanel('our_values'),
        FieldPanel('our_mission'),
        FieldPanel('our_vission'),
    ]

    def get_context(self, request):
        context = super().get_context(request)
        info = SiteInfo.objects.all()[0]
        context["info"] = info
        return context

    class Meta:
        verbose_name = "О компании"
        verbose_name_plural = "О компании"


class Contacts(Page):
    template = "home/contacts.html"

    content_panels = Page.content_panels + [

    ]

    def get_context(self, request):
        context = super().get_context(request)
        info = SiteInfo.objects.all()[0]
        context["info"] = info
        return context

    def serve(self, request):
        from .forms import ContactFormForm

        if request.method == 'POST':
            form = ContactFormForm(request.POST)
            if form.is_valid():
                print('Form OK')
                print(form.cleaned_data)

                obj = ContactFormModel(name=form.cleaned_data["name"],
                                       email=form.cleaned_data["email"],
                                       subject=form.cleaned_data["subject"],
                                       message=form.cleaned_data["message"])
                obj.save()
            else:
                print('Form NOT OK')
        else:
            form = ContactFormForm()
        form = ContactFormForm()
        return render(request, 'home/contacts.html', {
            'page': self,
            'form': form,
            "info": SiteInfo.objects.all()[0],
        })

    class Meta:
        verbose_name = "Контакты"
        verbose_name_plural = "Контакты"


class SignIn(Page):
    template = "home/sign_in.html"

    content_panels = Page.content_panels + [

    ]

    def serve(self, request):
        print(request.method)
        if request.user.is_authenticated:
            return HttpResponseRedirect('/')
        if request.method == 'GET':
            return super().serve(request)
        else:
            req = request.POST.copy()
            print(req)

            if 'register' in request.POST:
                print('Регистрация')
                email = req['remail']
                password = req['rpassword']
                inn = req['inn']
                name = req['name']
                print(email, password, inn, name)
                profile = authenticate(username=email, password=password)
                print('PROFILE', profile)
                if profile is not None:
                    return HttpResponseRedirect('/sign_in')
                else:
                    profile = User(email=email, password=make_password(password), inn=inn, name=name, username=email)
                    profile.save()
            else:
                # print(req['email'])
                print("Авторизация")
                email = req['lemail']
                password = req['lpassword']
                print(email, password)
                profile = authenticate(username=email, password=password)
                print('PROFILE', profile)
                if profile is not None:
                    print('GOOD', profile)
                    login(request, profile)
                    return HttpResponseRedirect('/')

            return HttpResponseRedirect('/sign_in')
            # if "func" in req:
            #     if req['func'] == 'sign_in':
            # Export to ical format
            # user = User.objects.get(email=req['email'], password=req['password'])

    def get_context(self, request):
        context = super().get_context(request)
        info = SiteInfo.objects.all()[0]
        context["info"] = info
        return context

    class Meta:
        verbose_name = "Вход и регистрация"
        verbose_name_plural = "Вход и регистрация"


class PersonalAccount(Page):
    template = "home/personal_account.html"

    content_panels = Page.content_panels + [

    ]

    def get_context(self, request):
        context = super().get_context(request)
        info = SiteInfo.objects.all()[0]
        context["info"] = info
        orders = []
        for order in ServiceOrder.objects.filter(user=request.user):
            serv = order.service
            service = {
                "id": order.id,

                "name": serv.name,
                "place_from": order.place_from,
                "place_to": order.place_to,
                "datetime": order.datetime
            }
            orders.append(service)
        context["orders"] = orders
        print(orders)
        return context

    class Meta:
        verbose_name = "Личный кабинет"
        verbose_name_plural = "Личный кабинет"


@register_snippet
class Messages(models.Model):
    order = models.ForeignKey(ServiceOrder, on_delete=models.CASCADE, related_name="+", verbose_name="Заказ", null=True,
                              blank=True)
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name="+", verbose_name="Отправитель", null=True,
                               blank=True)
    text = models.TextField(verbose_name="Текст сообщения", null=True, blank=True)
    datetime = models.DateTimeField(verbose_name="Дата-время", null=True, blank=True)

    panels = [
        SnippetChooserPanel('order'),
        FieldPanel('sender'),
        FieldPanel('text'),
        FieldPanel('datetime'),
        # FieldPanel('message')
    ]

    def __str__(self):
        return f"От {self.sender.email},  (" + self.text[:50] + "...)"

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"


@register_snippet
class Documents(models.Model):
    order = models.ForeignKey(ServiceOrder, on_delete=models.CASCADE, related_name="+", verbose_name="Заказ", null=True,
                              blank=True)
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name="+", verbose_name="Отправитель", null=True,
                               blank=True)
    document = models.ForeignKey(Document, on_delete=models.CASCADE, related_name="+", verbose_name="Документ",
                                 null=True,
                                 blank=True)
    # text = models.TextField(verbose_name="Текст сообщения", null=True, blank=True)
    # datetime = models.DateTimeField(verbose_name="Дата-время", null=True, blank=True)
    # link = models.CharField(max_length=40, null=True, blank=True, verbose_name="Ссылка")
    # name = models.TextField(max_length=20, null=True, blank=True, verbose_name="Название")

    panels = [
        FieldPanel('order'),
        FieldPanel('sender'),
        DocumentChooserPanel('document'),
    ]

    def __str__(self):
        return str(self.document) + ' для заказа ' + str(self.order.id)

    class Meta:
        verbose_name = "Документ для заказа"
        verbose_name_plural = "Документы для заказа"


#

class UploadFileForm(forms.Form):
    file = forms.FileField()


class MyOrders(Page):
    template = "home/my_orders.html"

    content_panels = Page.content_panels + [

    ]

    def handle_uploaded_file(self, f):
        filename = f"{uuid.uuid4()}_{f}"
        with open(f'media/documents/{filename}', 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)
        return filename, f

    def serve(self, request):
        print(request.method)
        if not request.user.is_authenticated:
            return HttpResponseRedirect('/')
        if request.method == 'GET':
            return super().serve(request)
        elif request.method == 'POST':
            data = request.POST.copy()
            # data = request.POST.copy()

            print('GOOD FILE')
            print("TYPE: ", request.POST.get('type'))
            print("GET", request.GET)
            print('POST', request.POST)

            id = request.GET.get("id")
            if request.POST.get('type') == 'file':
                print('FILES: ', request.FILES)
                print('FILE: ', request.FILES.get(request.POST.get('file')))
                file_form = UploadFileForm(request.POST, request.FILES)
                if file_form.is_valid():
                    filename = request.POST.get('file')
                    # self.handle_uploaded_file(request.FILES[filename], filename)
                    filename, f = self.handle_uploaded_file(request.FILES['file'])
                    base_doc = Document()
                    base_doc.file = f'documents/{filename}'
                    base_doc.title = f
                    base_doc.uploaded_by_user = request.user
                    base_doc.save()
                    document = Documents()
                    document.sender = request.user
                    document.order = ServiceOrder.objects.get(id=id)
                    document.document = base_doc
                    document.save()
                return HttpResponseRedirect(request.get_full_path())

            text = request.POST.get('text')
            if text != '':
                message = Messages(
                    sender=request.user,
                    order=ServiceOrder.objects.get(id=id),
                    text=text,
                    datetime=datetime.datetime.now(),
                )
                message.save()
            print("KEKOS", id)

        return super().serve(request)
        # if "func" in req:
        #     if req['func'] == 'sign_in':
        # Export to ical format
        # user = User.objects.get(email=req['email'], password=req['password'])

    def get_context(self, request):
        context = super().get_context(request)

        id = request.GET.get('id')
        if id is None:
            id = request.POST.get('id')
        print("ID ORDERS: ", id)
        order = ServiceOrder.objects.get(id=id)
        messages = Messages.objects.filter(order=order).order_by('datetime')
        documents = Documents.objects.filter(order=order)
        print(messages)
        context["messages"] = messages
        context["documents"] = documents
        print(documents)
        info = SiteInfo.objects.all()[0]
        context["info"] = info
        context["order"] = order
        context["upload_file_form"] = UploadFileForm()
        # print(context["orders"])
        return context

    class Meta:
        verbose_name = "Мои заказы"
        verbose_name_plural = "Мои заказы"
