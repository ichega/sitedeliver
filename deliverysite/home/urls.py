from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.urls import path
from .views import logout_view

urlpatterns = [
    path('logout/', logout_view)
]