# Generated by Django 2.2.1 on 2019-05-22 04:04

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0041_group_collection_permissions_verbose_name_plural'),
        ('home', '0015_serviceorder'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='aboutcompany',
            options={'verbose_name': 'О компании', 'verbose_name_plural': 'О компании'},
        ),
        migrations.AlterModelOptions(
            name='contacts',
            options={'verbose_name': 'Контакты', 'verbose_name_plural': 'Каонтакты'},
        ),
        migrations.AlterModelOptions(
            name='mainpage',
            options={'verbose_name': 'Главная', 'verbose_name_plural': 'Главные'},
        ),
        migrations.AlterModelOptions(
            name='productpage',
            options={'verbose_name': 'Страница описания услуги', 'verbose_name_plural': 'Страницы описания услуг'},
        ),
        migrations.AlterModelOptions(
            name='review',
            options={'verbose_name': 'Обзор', 'verbose_name_plural': 'Обзоры'},
        ),
        migrations.AlterModelOptions(
            name='reviews',
            options={'verbose_name': 'Обзор', 'verbose_name_plural': 'Обзоры'},
        ),
        migrations.AlterModelOptions(
            name='service',
            options={'verbose_name': 'Услуга', 'verbose_name_plural': 'Услуги'},
        ),
        migrations.AlterModelOptions(
            name='serviceorder',
            options={'verbose_name': 'Заказ услуги', 'verbose_name_plural': 'Заказы услуг'},
        ),
        migrations.AlterModelOptions(
            name='servicepage',
            options={'verbose_name': 'Услуга', 'verbose_name_plural': 'Услуги'},
        ),
        migrations.AlterModelOptions(
            name='signin',
            options={'verbose_name': 'Вход и регистрация', 'verbose_name_plural': 'Вход и регистрация'},
        ),
        migrations.AlterModelOptions(
            name='siteinfo',
            options={'verbose_name': 'Информация о сайте', 'verbose_name_plural': 'Информация о сайте'},
        ),
        migrations.AddField(
            model_name='servicepage',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailcore.Page', verbose_name='Страница-предок'),
        ),
        migrations.AlterField(
            model_name='aboutcompany',
            name='info',
            field=models.TextField(blank=True, max_length=4000, null=True, verbose_name='Информация'),
        ),
        migrations.AlterField(
            model_name='mainpage',
            name='delivery_text',
            field=models.TextField(blank=True, max_length=1000, null=True, verbose_name='Доставка'),
        ),
        migrations.AlterField(
            model_name='mainpage',
            name='rent_text',
            field=models.TextField(blank=True, max_length=1000, null=True, verbose_name='Аренда'),
        ),
        migrations.AlterField(
            model_name='review',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, verbose_name='Пользователь'),
        ),
        migrations.AlterField(
            model_name='service',
            name='description',
            field=models.TextField(blank=True, max_length=80, null=True, verbose_name='Описание'),
        ),
        migrations.AlterField(
            model_name='service',
            name='image',
            field=models.ImageField(upload_to='', verbose_name='Изображение'),
        ),
        migrations.AlterField(
            model_name='service',
            name='link_page',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='wagtailcore.Page', verbose_name='Расположить на странице'),
        ),
        migrations.AlterField(
            model_name='service',
            name='name',
            field=models.CharField(blank=True, max_length=80, null=True, verbose_name='Название'),
        ),
        migrations.AlterField(
            model_name='serviceorder',
            name='datetime',
            field=models.DateTimeField(verbose_name='Дата=время'),
        ),
        migrations.AlterField(
            model_name='serviceorder',
            name='place_from',
            field=models.CharField(max_length=80, verbose_name='Откуда'),
        ),
        migrations.AlterField(
            model_name='serviceorder',
            name='place_to',
            field=models.CharField(max_length=80, verbose_name='Куда'),
        ),
        migrations.AlterField(
            model_name='serviceorder',
            name='service',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='home.Service', verbose_name='Услуга'),
        ),
        migrations.AlterField(
            model_name='servicepage',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='', verbose_name='Изображение'),
        ),
        migrations.AlterField(
            model_name='siteinfo',
            name='address',
            field=models.TextField(blank=True, max_length=200, null=True, verbose_name='Адрес'),
        ),
        migrations.AlterField(
            model_name='siteinfo',
            name='email',
            field=models.EmailField(blank=True, max_length=40, null=True, verbose_name='Email'),
        ),
        migrations.AlterField(
            model_name='siteinfo',
            name='phone',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='Номер телефона'),
        ),
    ]
