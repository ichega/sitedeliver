from wagtail.users.forms import UserEditForm, UserCreationForm
from django import forms
from .models import *


class CustomUserEditForm(UserEditForm):
    inn = forms.CharField(required=False, label=("ИНН"))
    name = forms.CharField(required=False, label=("Название фирмы"))


    # email = forms.CharField(required=True, label=("email"))
    # password = forms.CharField(required=True, label=("password"))

class CustomUserCreationForm(UserCreationForm):
    inn = forms.CharField(required=False, label=("ИНН"))
    name = forms.CharField(required=False, label=("Название фирмы"))
    # email = forms.CharField(required=True, label=("email"))
    # password = forms.CharField(required=True, label=("password"))
